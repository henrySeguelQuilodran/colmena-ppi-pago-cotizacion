package page;




import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import logic.UtilityClass;



public class LoginPage extends UtilityClass {
	

	
	//Localizadores de Login Sucursal Virtual//
		
	By campoRutLoginSucursalVirtualSelector = By.xpath("//input[@placeholder='12.345.678-9']");
	By campoPasswordLoginSucursalVirtualSelector = By.xpath("//input[@type='password']");
	By btnLoginSucursalVirtualSelector = By.xpath("//button[@type='submit']");
	
	

	
	public LoginPage(WebDriver driver) {
		super(driver);
		
	}

	public void loginEnSucursalVirtual() throws InterruptedException {
		

		type("42277762",campoRutLoginSucursalVirtualSelector);
		Thread.sleep(3000);	
		type("prueba01",campoPasswordLoginSucursalVirtualSelector);
		Thread.sleep(3000);
		click(btnLoginSucursalVirtualSelector);
		
		}		
	
public void ingresoSucursalVirtual() throws InterruptedException {
		

		click(btnLoginSucursalVirtualSelector);
		
		}		
	
	
}	
	

