package logic;


import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
//import org.openqa.selenium.firefox.FirefoxDriver;

public class UtilityClass {
	
	protected WebDriver driver;
	
	public UtilityClass(WebDriver driver) {
		this.driver = driver;
	}

	public WebDriver driverConexion() {
		System.setProperty("webdriver.chrome.driver", "drivers/chromedriver.exe");
		driver = new ChromeDriver();
		//System.setProperty("webdriver.gecko.driver", "drivers/geckodriver.exe");
		//driver = new FirefoxDriver();	
		driver.manage().window().maximize();
		driver.manage().deleteAllCookies();
		return driver;
	}
	
	//wrapping(envoltorio) de metodos de selenium
	public WebElement findElement(By locator) {
		return driver.findElement(locator);	//--> llamamos los metodos de la api de Selenium WebDriver
		}
	//metodo que nos va a devolver una lista de elementos
	//"By locator" --> es pasar un parametro localizador
	public List<WebElement> findElements(By locator){
		return driver.findElements(locator);
	}
	//metodo que devuelve el texto del elemento parametro
	public String getText(WebElement element) {
		return element.getText();
	}
	//nos va a delvolver el texto del elemento que estamos buscando
	//a traves de ese localizador
	public String getText(By locator) {
		return driver.findElement(locator).getText();		
	}
	//metodo para escribir texto con localizador del elemento
	public void type(String inputText, By locator) {
		driver.findElement(locator).sendKeys(inputText);
	}
	//metodo click
	public void click(By locator) {
		driver.findElement(locator).click();
	}
	//metodo booleano que nos va a decir si el elemento se encuentra listo para ser utilizado
	//esto nos retornara verdadero o falso
	public Boolean isDisplayed(By locator) {
		try {
			return driver.findElement(locator).isDisplayed();
		}catch(org.openqa.selenium.NoSuchElementException e) {
			return false;
		}	 
	}
	//metodo encargado de recibir la URL y abrir la pagina
	public void visit(String url) {
		driver.get(url);
	}
	

}
