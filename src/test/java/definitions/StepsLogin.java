package definitions;

import org.openqa.selenium.WebDriver;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.When;
import logic.UtilityClass;
import page.LoginPage;

public class StepsLogin {
	

	WebDriver driver;
	UtilityClass utilityClass = new UtilityClass(driver);
	LoginPage loginPage = new LoginPage(utilityClass.driverConexion());
	
	
	//Gherkin
	
	@Given("El usuario va al area de sucursal virtual")
	public void el_usuario_va_al_area_de_sucursal_virtual() {

		String URL = "http://190.215.119.52:8180/afiliados/#/login";
		//String URL = "https://www.colmena.cl/afiliados/#/login";
		utilityClass.visit(URL);
		
	}
	@Given("Ingresa su rut y contrasenia")
	public void ingresa_su_rut_y_contrasenia() throws InterruptedException {
	  
		loginPage.loginEnSucursalVirtual();
	}
	
	@When("Usuario logra acceso a Sucursal Virtual")
	public void usuario_logra_acceso_a_sucursal_virtual() throws InterruptedException {
	 
		loginPage.ingresoSucursalVirtual();
	}


}